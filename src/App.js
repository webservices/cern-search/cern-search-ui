import React, {Component} from 'react';
import {
    Error,
    InvenioSearchApi,
    ReactSearchKit,
    ResultsLoader,
    withState
} from 'react-searchkit';
import {Container, Grid} from "semantic-ui-react";
import {Results} from "./components/Results";
import CernHeader from "./components/Header";
import {InvenioRequestSerializer} from "./InvenioRequestSerializer";
import {SearchHeader} from "./components/SearchHeader";
import {MenuFilter} from "./components/MenuFilter";
import {AutocompleteApi} from "./AutocompleteApi";
import {NoResults} from "./components/NoResults";

const searchApi = new InvenioSearchApi({
    axios: {
        url: `${process.env.REACT_APP_BACKEND_BASE_URL}/api/records`,
        timeout: 60000,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        params: {
            highlight: "*"
        }
    },
    invenio: {
        requestSerializer: InvenioRequestSerializer,
    },
});

const suggesterApi = new AutocompleteApi({
    axios: {
        url: `${process.env.REACT_APP_BACKEND_BASE_URL}/api/records`,
        timeout: 60000,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }
});

const OnSearchHeader = withState(SearchHeader);
const StateFullMenuFilter = withState(MenuFilter);

class App extends Component {
    render() {
        const sortByValues = [
            {
                text: 'Best Match',
                value: 'bestmatch',
            },
            {
                text: 'Newest',
                value: 'mostrecent',
            },
        ];

        const sortOrderValues = [
            {text: 'asc', value: 'asc'},
            {text: 'desc', value: 'desc'},
        ];

        const resultsPerPageValues = [
            {
                text: '10',
                value: 10,
            },
            {
                text: '20',
                value: 20,
            },
            {
                text: '50',
                value: 50,
            },
        ];

        return (
            <ReactSearchKit searchApi={searchApi}>
                <div>
                    <CernHeader/>
                    <OnSearchHeader/>
                    <Container>
                        <Grid relaxed centered style={{marginTop: '2em'}}>
                            <StateFullMenuFilter suggesterApi={suggesterApi}/>
                            <Grid.Column width={12}>
                                <Grid.Row>
                                    <ResultsLoader>
                                        <NoResults/>
                                        <Error/>
                                        <Results
                                            sortByValues={sortByValues}
                                            sortOrderValues={sortOrderValues}
                                            resultsPerPageValues={resultsPerPageValues}
                                        />
                                    </ResultsLoader>
                                </Grid.Row>
                            </Grid.Column>
                        </Grid>
                    </Container>
                </div>
            </ReactSearchKit>
        );
    }
}

export default App;