/*
 * This file is part of React-SearchKit.
 * Copyright (C) 2019 CERN.
 *
 * React-SearchKit is free software; you can redistribute it and/or modify it
 * under the terms of the MIT License; see LICENSE file for more details.
 */

import {InvenioSearchApi} from "react-searchkit";


/** Default suggestions response serializer */
class AutocompleteAggsResponseSerializer {
  _serializeSuggestions = aggregations => {
    return aggregations;
  };

  /**
   * Return a serialized version of the API backend response for the app state `suggestions`.
   * @param {object} payload the backend response payload
   */
  serialize = payload => {
    return this._serializeSuggestions(payload.aggregations || [])
  };
}

/** Default Invenio Suggestion API adapter */
export class AutocompleteApi extends InvenioSearchApi {
  constructor(config = {}) {
    super(config);
    this.filters = config.filters;


    this.responseSerializer =
      config.responseSerializer ||
      new AutocompleteAggsResponseSerializer();
  }
}