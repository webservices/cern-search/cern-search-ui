import React, {Component} from 'react'
import {Dropdown} from 'semantic-ui-react'
import {isEmpty, match} from "./../utils"
import _cloneDeep from 'lodash/cloneDeep';
import parse from 'autosuggest-highlight/parse';
import memoize from "memoize-one";

export class AutocompleteFilter extends Component {
    constructor(props) {
        super(props);

        this.onSearchChange = this.onSearchChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onClose = this.onClose.bind(this);

        this.buildNewFilters = props.buildNewFilters || this._buildNewFilters;
        this.selection = props.selection || this._selection;
        this.additions = props.additions || this._additions;

        this.filter = props.filter;

        this.state = {tmpAggregations: null, dropdownSearchQuery: null, loading: false, additions: []};
    }

    async onSearchChange(e, {searchQuery}) {
        this.setState({searchQuery});
    }

    fetchOptions(searchQuery) {
        if (searchQuery) {
            this.setState({loading: true});

            let tempState = _cloneDeep(this.props.currentQueryState);
            tempState.filters = tempState.filters.filter(el => el[0] !== this.filter.queryField);
            tempState.filters.push([this.filter.suggesterField, searchQuery.toLowerCase()]);

            this.props.suggesterApi.search(tempState)
                .then(({[this.filter.responseField]: {buckets}}) => {
                    this.setState({tmpAggregations: buckets, loading: false})
                });

            return
        }

        this.setState({tmpAggregations: null});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.searchQuery !== prevState.searchQuery) {
            this.fetchOptions(this.state.searchQuery);
        }
    }

    onClose(event, data) {
        this.setState({tmpAggregations: null, searchQuery: null})
    }

    _buildNewFilters(filter, field) {
        return [[field, filter]];
    }

    _buildQueryFilters(selection, field) {
        const stateFilters = _cloneDeep(this.props.currentQueryState.filters);
        const previousFilters = stateFilters.filter(el => el[0] === field);

        return  selection
            ? [...this.buildNewFilters(selection, field), ...previousFilters]
            : previousFilters;
    }

    onChange(event, data) {
        this.props.updateQueryState({filters: this._buildQueryFilters(data.value, this.filter.queryField), page: 1});
        this.setState((state) => ({
            tmpAggregations: null,
            searchQuery: null,
        }));
    }

    _valueLabel(label, search) {
        const matches = match(label, search);
        const parts = parse(label, matches);

        return parts.map((part, index) =>
            part.highlight ? (
                <em>{part.text}</em>
            ) : (
                part.text
            )
        );
    }

    _buildOptions(buckets) {
        return buckets.map(bucket => {
            let content =
                <span style={{textTransform: 'capitalize'}}>{this._valueLabel(bucket, this.state.searchQuery)}</span>;

            return ({
                key: bucket,
                text: bucket,
                value: bucket,
                content: (
                    content
                ),
            });
        });
    }

    _additions = memoize((selection, buckets) => {
       return buckets.includes(selection) ? [] : [selection];
    });

    options = memoize((tmp, aggs, selection) => {
        let buckets = tmp;
        if (!tmp && !isEmpty(aggs)) {
            buckets = aggs[this.filter.responseField].buckets;
        }

        buckets = buckets.map((bucket => bucket.key));

        if (selection) {
            buckets = [...this.additions(selection, buckets), ...buckets];
        }

        return buckets ? this._buildOptions(buckets) : [];
    });

    _selection = memoize((filters) => {
        const filterValue = filters.find(element => element[0] === this.filter.queryField);
        if (filterValue) {
            return filterValue[1];
        }

        return null;
    });

    render() {
        const selection = this.selection(this.props.currentQueryState.filters);
        const options = this.options(this.state.tmpAggregations, this.props.currentResultsState.data.aggregations, selection);

        return (
                <Dropdown
                    style={{textTransform: 'capitalize'}}
                    deburr
                    multiple={this.props.multiple}
                    fluid
                    floating
                    clearable
                    selection
                    options={options}
                    search
                    closeOnEscape={true}
                    selectOnNavigation={false}
                    selectOnBlur={false}
                    closeOnChange={true}
                    defaultOpen={false}
                    value={selection}
                    text={selection && selection.length > 0 ? selection : null}
                    loading={this.state.loading}
                    additionLabel={<i style={{color: 'red'}}>Not Found: </i>}
                    placeholder={"Filter..."}
                    onSearchChange={this.onSearchChange}
                    onChange={this.onChange}
                    onClose={this.onClose}
                />
        )
    }
}
