import React from 'react';
import { Segment, Container, Header, Grid } from 'semantic-ui-react';

const CernHeader = () => {
    return (
        <Segment inverted className="no-border-radius no-margin cern-header">
            <Container>
                <Grid columns="equal" verticalAlign="middle">
                    <Grid.Row>
                        <Grid.Column width="3" textAlign='right' className="no-padding">
                            <a href='/'>
                                <Header as="h4" inverted>
                                    CERN
                                    <span style={{color: '#999'}}> Accelerating Science </span>
                                </Header>
                            </a>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </Segment>
    )
}

export default CernHeader;
