import React, {Component} from 'react'
import {Accordion, Card, Divider, Grid, Header} from 'semantic-ui-react'
import {BucketAggregation, withState} from "react-searchkit";
import {AutocompleteFilter} from "./AutocompleteFilter";
import {MultiAutocompleteFilter} from "./MultiAutocompleteFilter";
import {TextFilter} from "./TextFilter";

const StateFullAutocompleteFilter = withState(AutocompleteFilter);
const StateFullMultiAutocompleteFilter = withState(MultiAutocompleteFilter);
const StateFullTextFilter = withState(TextFilter);

export class MenuFilter extends Component {
    render() {
        let panels = [
            {
                key: 'details',
                title: "More filters",
                content: {
                    content: (
                        <>
                            <Header>Keywords</Header>
                            <StateFullTextFilter filter={{matchField: "keyword_match"}}/>
                            <Divider horizontal>Or</Divider>
                            <StateFullMultiAutocompleteFilter
                                filter={{
                                    queryField: "keyword",
                                    suggesterField: "keywords_suggest",
                                    responseField: "keyword"

                                }}
                                suggesterApi={this.props.suggesterApi}
                            />


                            <Divider hidden/>

                            <Header>Authors</Header>
                            <StateFullTextFilter filter={{matchField: "author_match"}}/>
                            <Divider horizontal>Or</Divider>
                            <StateFullAutocompleteFilter
                                filter={{
                                    queryField: "author",
                                    suggesterField: "authors_suggest",
                                    responseField: "author"
                                }}
                                suggesterApi={this.props.suggesterApi}
                            />

                            <Divider hidden/>

                            <Header>Title</Header>
                            <StateFullTextFilter filter={{matchField: "name_match"}}/>
                        </>
                    ),
                },
            },
        ];

        const hasResults = !this.props.currentResultsState.loading && this.props.currentResultsState.data.total;

        return hasResults ? (
            <Grid.Column width={4}>
                <Card.Group>
                    <BucketAggregation style={{textTransform: 'capitalize'}} title="Collection"
                                       agg={{field: "collection", aggName: 'collection'}}/>
                    <BucketAggregation title="Type"
                                       agg={{field: "type_format", aggName: 'type_format'}}/>
                    <BucketAggregation title="Site"
                                       agg={{field: "site", aggName: 'site'}}/>
                    <Card>
                        <Card.Content>
                            <Accordion fluid defaultActiveIndex={0} panels={panels}/>
                        </Card.Content>
                    </Card>
                </Card.Group>
            </Grid.Column>
        ) : null;
    }
}
