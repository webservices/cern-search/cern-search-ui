import React, {Component} from 'react'
import {AutocompleteFilter} from "./AutocompleteFilter";
import {withState} from "react-searchkit";
import memoize from "memoize-one";

const StateFullAutocompleteFilter = withState(AutocompleteFilter);

export class MultiAutocompleteFilter extends Component {
    constructor(props) {
        super(props);

        this.filter = props.filter;
    }

    render() {
        const buildNewFilters = (filter, field) => {
            if (Array.isArray(filter)) {
                return filter.map((entry) => [field, entry], this);
            }

            return [[field, filter]];
        };

        const selection = memoize((filters) => {
            return filters.reduce(function (filtered, element) {
                if (element[0] === this.filter.queryField) {
                    filtered.push(element[1]);
                }

                return filtered;
            }.bind(this), []);
        });

        const additions = memoize((selection, buckets) => {
           return selection.filter((selected) => !buckets.includes(selected))
        });

        return (
            <StateFullAutocompleteFilter
                buildNewFilters={buildNewFilters}
                selection={selection}
                additions={additions}
                suggesterApi={this.props.suggesterApi}
                filter={this.props.filter}
                multiple
            />
        )
    }
}
