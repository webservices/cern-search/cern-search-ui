import {Button, Grid, Header, Icon, Label, Segment} from "semantic-ui-react";
import {ActiveFilters, EmptyResults} from "react-searchkit";
import React, {Component} from 'react'


export class NoResults extends Component {
    render() {
        const _getLabel = filter => {
            const aggName = filter[0];
            let value = filter[1];
            let currentFilter = [aggName, value];
            const hasChild = filter.length === 3;
            if (hasChild) {
                const {label, activeFilter} = this._getLabel(filter[2]);
                value = `${value}.${label}`;
                currentFilter.push(activeFilter);
            }
            return {
                name: aggName,
                value,
                activeFilter: currentFilter,
            };
        };

        const renderActiveFilters = (filters, removeActiveFilter) => {
            return filters.map((filter, index) => {
                const {name, value, activeFilter} = _getLabel(filter);

                const styled = name === "author";
                return (
                    <Label as='a' icon key={index} onClick={() => removeActiveFilter(activeFilter)}>
                        <span style={{textTransform: 'capitalize'}}>{name}: </span>
                        <span style={{textTransform: styled ? 'capitalize' : 'none'}}>{value}</span>
                        <Icon name="delete"/>
                    </Label>
                );
            });
        };

        const renderEmptyResults = (queryString, resetQuery) => (
            <Segment placeholder textAlign="center">
                <Grid relaxed padded centered>
                    <Grid.Row>
                        <Header icon> <Icon name="search"/>
                            No results found!
                        </Header>
                    </Grid.Row>
                    <Grid.Row>
                        <em>Current search: {queryString}</em>
                    </Grid.Row>
                    <Grid.Row>
                        <Label.Group>
                            <ActiveFilters renderElement={renderActiveFilters}/>
                        </Label.Group>
                    </Grid.Row>
                    <Grid.Row>
                        <Button primary onClick={() => resetQuery()}>
                            Clear query
                        </Button>
                    </Grid.Row>
                </Grid>
            </Segment>
        );

        return (
            <EmptyResults renderElement={renderEmptyResults}/>
        )
    }
}