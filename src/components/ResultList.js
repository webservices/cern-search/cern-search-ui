/*
 * This file is part of CERN Search Web UI.
 * Copyright (C) 2018 CERN.
 *
 * React-SearchKit is free software; you can redistribute it and/or modify it
 * under the terms of the MIT License; see LICENSE file for more details.
 */

import React from 'react';
import _truncate from 'lodash/truncate';
import {Card, Grid, Icon, Image, Item} from 'semantic-ui-react';


const sanitizeHtml = require('sanitize-html');
const HtmlToReactParser = require('html-to-react').Parser;
const htmlToReactParser = new HtmlToReactParser();

const renderNormalResult = (metadata, index, highlight) => {
    const icon = icon_from_meta(metadata.collection);
    const authors = highlight["_data.authors"] ? htmlToReactParser.parse(sanitizeHtml(highlight["_data.authors"])) : metadata._data.authors;
    const content = highlight["_data.content"] ?
        prepareHighlights(highlight["_data.content"]) :
        _truncate(metadata._data.content, {length: 300});
    const header = _truncate(metadata._data.name.trim(), {length: 80}) || metadata.file;

    switch (metadata.collection) {
        case 'Official':
        default:
            return (
                <Item key={index} href={`https://${metadata.url}`} target="_blank">
                    <Item.Content>
                        <Item.Header>
                            {metadata.file && icon &&
                            <Icon color={icon.color} name={icon.name}/>
                            }
                            {header}
                        </Item.Header>
                        {authors &&
                        <Item.Meta>
                            <span className='authors'>
                              Authored by {authors}
                            </span>
                        </Item.Meta>
                        }
                        <Item.Description> {content} </Item.Description>
                        <Item.Extra>{`${metadata.url}`}</Item.Extra>
                    </Item.Content>
                </Item>
            )
    }
};

const icon_from_meta = (collection) => {
    switch (collection) {
        case 'PDF':
            return {name: 'file pdf outline', color: 'red'};
        case 'Document':
            return {name: 'file word outline', color: 'blue'};
        case 'Slides':
            return {name: 'file powerpoint outline', color: 'orange'};
        case 'Sheet':
            return {name: 'file excel outline', color: 'green'};
        case 'Other':
            return {name: 'file outline', color: 'gray'};
        case 'Archive':
            return {name: 'file archive outline', color: 'gray'};
        default:
            return null;
    }
};

const prepareHighlights = (highlights) => {
    let highlight = highlights[0];
    if (highlights[1]) {
        highlight += " ... " + highlights[1]
    }
    if (highlights[2]) {
        highlight += " ... " + highlights[2]
    }
    highlight += " ... ";

    return htmlToReactParser.parse(sanitizeHtml(highlight))
};

const renderPromotedResult = (metadata, index, highlight) => {
    switch (metadata.collection) {
        case 'Official':
        default:
            return (
                <Card key={index} fluid href={`https://${metadata._data.url}`}>
                    <Card.Content>
                        <Image src={metadata.image_source} size='small' floated="left"/>
                        <Card.Header>{metadata._data.name}</Card.Header>
                        <Card.Description style={{clear: 'none'}}>
                            {_truncate(metadata._data.content, {length: 200})}
                        </Card.Description>
                        <Card.Content extra>
                            {`${metadata._data.url}`}
                        </Card.Content>
                    </Card.Content>
                </Card>
            )
    }
};

export const renderResultList = results => {
    let promoted = []
    let normal = []

    results.forEach((result, index) => {
            const metadata = result.metadata;
            const highlight = result.highlight;

            if (!metadata.promoted) {
                normal.push(renderNormalResult(metadata, index, highlight))
            } else {
                promoted.push(renderPromotedResult(metadata, index, highlight))
            }
        }
    );

    return (
        <>
            {promoted.length > 0 &&
            <Grid.Row>
                <Card.Group>
                    {promoted}
                </Card.Group>
            </Grid.Row>
            }
            <Grid.Row>
                <Item.Group divided relaxed='very' link>
                    {normal}
                </Item.Group>
            </Grid.Row>
        </>
    );
};
