/*
 * This file is part of CERN Search UI.
 * Copyright (C) 2019 CERN.
 *
 * CERN Search UI is free software; you can redistribute it and/or modify it
 * under the terms of the MIT License; see LICENSE file for more details.
 */

import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import {
  connect,
  Count,
  Pagination,
  ResultsList,
  ResultsPerPage,
  SortBy,
  SortOrder,
} from 'react-searchkit';
import { renderResultList }  from './ResultList';

const Spacer = connect(state => ({
  loading: state.results.loading,
  total: state.results.data.total,
}))(({ text, loading, total }) =>
  loading || total === 0 ? null : (
    <span style={{ margin: '0 0.5em' }}>{text}</span>
  )
);

export class Results extends Component {
  constructor(props) {
    super(props);

    this.sortByValues = this.props.sortByValues;
    this.sortOrderValues = this.props.sortOrderValues;
    this.resultsPerPageValues = this.props.resultsPerPageValues;
  }

  render() {
    return (
      <div>
        <Grid relaxed verticalAlign="middle">
          <Grid.Column width={13}>
            <span>
              <Spacer text="Found" />
              <Count />
              <Spacer text="results sorted by" />
              <SortBy
                values={this.sortByValues}
                defaultValue="bestmatch"
                defaultValueOnEmptyString="bestmatch"
              />
              <SortOrder values={this.sortOrderValues} defaultValue="desc" />
            </span>
          </Grid.Column>
          <Grid.Column width={3} textAlign="right">
            <span>
              <Spacer text="Show" />
              <ResultsPerPage
                values={this.resultsPerPageValues}
                defaultValue={10}
              />
            </span>
            <Spacer />
          </Grid.Column>
        </Grid>
        <Grid relaxed>
          <Grid.Column width={13}>
            <ResultsList renderElement={ renderResultList }/>
          </Grid.Column>
        </Grid>
        <Grid relaxed verticalAlign="middle" textAlign="center">
          <Pagination
            options={{ showEllipsis: true, showLastIcon: true }}
            defaultValue={1}
          />
        </Grid>
      </div>
    );
  }
}

Results.propTypes = {};

Results.defaultProps = {};
