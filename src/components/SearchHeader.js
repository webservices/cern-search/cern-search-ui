import React, {Component} from 'react';
import {Container, Grid, Image, Menu, Segment} from 'semantic-ui-react';
import {AutocompleteSearchBar, BucketAggregation} from "react-searchkit";
import logo from '../assets/logo-white.png';


const _renderMenuAggregation = (
  title,
  resultsAggregations,
  aggregations,
  customProps
) => {

  const activeItem = 'All'

  return resultsAggregations !== undefined ? (
      <Menu inverted pointing secondary>
        <Menu.Item
          name='All'
          active={activeItem === 'All'}
        />
        <Menu.Item
          name='Web'
          active={activeItem === 'Web'}
        />
        <Menu.Item
          name='Documentation'
          active={activeItem === 'Documentation'}
        />
      </Menu>
  ): null;
};

const HeaderFilter = () => {
  return (
    <BucketAggregation
      title="Site Type"
      agg={{field:"collection", aggName: 'collection'}}
      renderElement={_renderMenuAggregation}
    />
  )
}

export class SearchHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Segment color="blue" inverted className="no-border-radius no-margin">
                <Container>
                    <Grid verticalAlign="middle" relaxed>
                        <Grid.Row>
                            <Grid.Column width={4}>
                                <a href='/'>
                                    <Image centered src={logo} size="medium"/>
                                </a>
                            </Grid.Column>
                            <Grid.Column width={12} textAlign="center">
                                <Grid style={{padding: "2rem 0"}}>
                                    <Grid.Column>
                                        <Grid.Row>
                                            <AutocompleteSearchBar/>
                                        </Grid.Row>
                                        <Grid.Row>
                                            <HeaderFilter/>
                                        </Grid.Row>
                                    </Grid.Column>
                                </Grid>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </Segment>
        )
    }
}


SearchHeader.propTypes = {};

SearchHeader.defaultProps = {};