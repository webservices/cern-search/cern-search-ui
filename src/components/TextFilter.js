import React, {Component} from 'react'
import {Input} from 'semantic-ui-react'
import _cloneDeep from 'lodash/cloneDeep';
import memoize from "memoize-one";

export class TextFilter extends Component {
    constructor(props) {
        super(props);

        this.onOpenTextClick = this.onOpenTextClick.bind(this);
        this.onOpenTextChange = this.onOpenTextChange.bind(this);

        this.filter = props.filter;

        this.state = {query: null};
    }

    onOpenTextChange = e => {
        this.setState({query: e.target.value});
    };


    onOpenTextClick() {
        const openText = this._opentext(this.props.currentQueryState.filters, this.state.query);

        //mitigation of https://github.com/inveniosoftware/react-searchkit/blob/v0.18.0/src/lib/state/selectors/query.js#L56
        const current = this._currentFilterValue(this.props.currentQueryState.filters);
        if (current && openText && (openText.startsWith(current) || current.startsWith(openText))) {
            // Actively remove filter
            this.props.updateQueryState({filters: this.buildNewFilters(current, this.filter.matchField)});
            // Add filter
            this.props.updateQueryState({filters: this.buildNewFilters(openText, this.filter.matchField), page: 1});

            return
        }

        this.props.updateQueryState({filters: this._buildQueryFilters(openText, this.filter.matchField), page: 1});
    }

    buildNewFilters(filter, field) {
        return [[field, filter]];
    }

    _buildQueryFilters(selection, field) {
        const stateFilters = _cloneDeep(this.props.currentQueryState.filters);
        const previousFilters = stateFilters.filter(el => el[0] === field);
        debugger
        return selection
            ? [...this.buildNewFilters(selection, field), ...previousFilters]
            : previousFilters;
    }

    _opentext = memoize((filters, openSearchQuery) => {
        if (openSearchQuery !== null) {
            return openSearchQuery;
        }

        return this._currentFilterValue(filters);
    });

    _currentFilterValue(filters = []) {
        const filterValue = filters.find(element => element[0] === this.filter.matchField);
        if (filterValue) {
            return filterValue[1];
        }

        return null;
    }

    render() {
        const openText = this._opentext(this.props.currentQueryState.filters, this.state.query);

        return (
            <Input
                fluid
                action={{
                    icon: 'search',
                    onClick: () => this.onOpenTextClick()
                }}
                placeholder='Search something...'
                onChange={this.onOpenTextChange}
                defaultValue={openText}
            />
        )
    }
}
