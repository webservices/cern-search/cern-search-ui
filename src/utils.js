const diacritic = require("diacritic");

export function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
/* TODO
Remove when https://www.npmjs.com/package/autosuggest-highlight supports match everywhere in word.
Read more: https://github.com/moroshko/autosuggest-highlight/issues/5#issuecomment-618086590
*/
export function match(text, query) {
    if (!("string" === typeof query && "string" === typeof text)) {
        return [];
    }

    let t = text.toLowerCase();
    let q = query.trim().toLowerCase();
    const ql = q.length;

    if (!(0 < ql && 0 < t.length)) {
        return [];
    }

    t = diacritic.clean(t);
    q = diacritic.clean(q);

    let r = [];

    let i = t.indexOf(q);
    while (i !== -1) {
        r.push([i, i + ql]);
        i = t.indexOf(q, i + ql);
    }

    return r;
}